import config
from database.db import run_db, stop_db
from fastapi import FastAPI
from routers.dishes.routes import router as dishes_router

application: FastAPI = FastAPI(
    DEBUG=config.DEBUG,
    title=config.TITLE,
    description=config.DESCRIPTION,
    version=config.VERSION,
)

application.include_router(dishes_router)


@application.on_event("startup")
async def startup() -> None:
    await run_db()


@application.on_event("shutdown")
async def shutdown() -> None:
    await stop_db()
