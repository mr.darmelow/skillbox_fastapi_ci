from typing import AsyncGenerator

from config import DATABASE_URL
from database.models import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

engine = create_async_engine(DATABASE_URL)

async_session_maker = async_sessionmaker(engine, expire_on_commit=False)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as async_session:
        yield async_session


async def run_db() -> None:
    async with engine.connect() as connect:
        await connect.run_sync(BaseModel.metadata.create_all)


async def stop_db() -> None:
    await engine.dispose()
