import json

from pydantic import BaseModel, field_validator


class BaseDishSchema(BaseModel):
    name: str


class PreviewDishSchema(BaseDishSchema):
    id: int
    cooking_time: int
    views_quantity: int


class DishSchema(BaseDishSchema):
    description: str
    ingredients: list
    cooking_time: int


class DishSchemaIn(DishSchema):
    @field_validator("ingredients", mode="after")
    def _validator(cls, value: list) -> str:
        return json.dumps(value)


class DishSchemaOut(DishSchema):
    id: int

    class ConfigDict:
        from_attributes: bool = True

    @field_validator("ingredients", mode="before")
    def _validator(cls, value: str) -> list:
        return json.loads(value)


class SuccessfulResponseSchema(BaseModel):
    ok: bool
    data: dict
