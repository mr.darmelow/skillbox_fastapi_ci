import datetime
from typing import Any

from database.models import BaseModel
from routers.dishes.config import MAX_TIMEDELTA
from routers.users.models import UserModel
from sqlalchemy import ForeignKey, func
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column


class DishModel(BaseModel):
    __tablename__ = "dishes"

    name: Mapped[str]
    description: Mapped[str]
    cooking_time: Mapped[int]
    ingredients: Mapped[str]


class DishViewModel(BaseModel):
    __tablename__ = "dishes_views"

    dish_id: Mapped[int] = mapped_column(ForeignKey("dishes.id"), nullable=False)
    user_id: Mapped[int] = mapped_column(ForeignKey(UserModel.id), nullable=True)


class DishStatisticsModel(BaseModel):
    __tablename__ = "dishes_statistics"

    dish_id: Mapped[int] = mapped_column(ForeignKey("dishes.id"), nullable=False)
    views_quantity: Mapped[int]
    last_update_datetime: Mapped[datetime.datetime] = mapped_column(
        default=datetime.datetime.utcnow()
    )

    @hybrid_property
    def check_datetime_out(self) -> Any:
        time_stamp = func.julianday(self.last_update_datetime)
        time_ago = func.julianday(
            datetime.datetime.utcnow() - datetime.timedelta(minutes=MAX_TIMEDELTA)
        )
        return time_stamp < time_ago
