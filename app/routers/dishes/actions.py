import datetime
from typing import Any, Sequence

from routers.dishes.models import DishModel, DishStatisticsModel, DishViewModel
from routers.dishes.schemas import DishSchemaIn, DishSchemaOut
from sqlalchemy import Row, Select, Subquery, Update, func, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql.selectable import ScalarSelect


async def get_dishes(session: AsyncSession) -> Sequence[Row[Any]]:
    # информация о рецептах с количеством их просмотров
    subquery: Subquery = (
        select(
            DishModel.id,
            DishModel.name,
            DishModel.cooking_time,
            DishStatisticsModel.views_quantity,
        )
        .join_from(
            DishModel, DishStatisticsModel, DishModel.id == DishStatisticsModel.dish_id
        )
        .order_by(DishStatisticsModel.views_quantity.desc())
        .order_by(DishModel.cooking_time)
        .subquery()
    )

    query: Select = select(subquery)

    response = await session.execute(query)
    return response.fetchall()


async def get_dish(dish_id: int, user_id: int, session: AsyncSession) -> DishSchemaOut:
    # Добавляем запись о просмотре рецепта
    session.add(DishViewModel(dish_id=dish_id, user_id=user_id))

    # Проверяем нужно ли обновить (пересчитать) количество просмотров
    is_datetime_out_query: Select = select(
        DishStatisticsModel.check_datetime_out
    ).where(DishStatisticsModel.dish_id == dish_id)

    is_datetime_out: bool = await session.scalar(is_datetime_out_query)

    if is_datetime_out:
        # считаем количество просмотров рецепта
        views_subquery: ScalarSelect = (
            select(func.count().label("views_quantity"))
            .select_from(DishViewModel)
            .where(DishViewModel.dish_id == dish_id)
            .scalar_subquery()
        )

        # обновляем количество просмотров и дату со временем последнего обновления
        views_update_statement: Update = (
            update(DishStatisticsModel)
            .where(DishStatisticsModel.dish_id == dish_id)
            .values(
                views_quantity=views_subquery,
                last_update_datetime=datetime.datetime.utcnow(),
            )
        )
        await session.execute(views_update_statement)
        await session.flush()

    # полная информация о рецепте
    query: Select = select(DishModel).where(DishModel.id == dish_id)
    response = await session.scalar(query)

    await session.commit()

    return response


async def add_dish(data: DishSchemaIn, session: AsyncSession) -> int:
    # добавляем новое блюдо
    instance = DishModel(**data.model_dump())
    session.add(instance)
    await session.flush()

    # добавляем запись статистики об этом блюде
    session.add(DishStatisticsModel(dish_id=instance.id, views_quantity=0))
    await session.commit()

    return instance.id
