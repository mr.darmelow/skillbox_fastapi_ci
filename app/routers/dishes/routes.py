from database.db import get_async_session
from fastapi import APIRouter, Depends
from routers.dishes.actions import add_dish, get_dish, get_dishes
from routers.dishes.schemas import (
    DishSchemaIn,
    DishSchemaOut,
    PreviewDishSchema,
    SuccessfulResponseSchema,
)
from sqlalchemy.ext.asyncio import AsyncSession

router: APIRouter = APIRouter(
    prefix="/dishes", tags=["dishes"], responses={404: {"description": "Not found"}}
)


@router.get("/", response_model=list[PreviewDishSchema])
async def _get_dishes(
    session: AsyncSession = Depends(get_async_session),
) -> list[PreviewDishSchema]:
    """
    Endpoint for receiving dishes
    """
    return await get_dishes(session)


@router.post("/", response_model=SuccessfulResponseSchema, status_code=201)
async def _add_dish(
    data: DishSchemaIn, session: AsyncSession = Depends(get_async_session)
) -> dict:
    """
    Endpoint for adding a new dish
    """
    return {"ok": True, "data": {"id": await add_dish(data, session)}}


@router.get("/{dish_id}", response_model=DishSchemaOut | None)
async def _get_dish(
    dish_id: int, user_id: int, session: AsyncSession = Depends(get_async_session)
) -> DishSchemaOut | None:
    """
    Endpoint for receiving a dish by id
    """
    return await get_dish(dish_id=dish_id, user_id=user_id, session=session)
