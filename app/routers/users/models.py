from database.models import BaseModel


class UserModel(BaseModel):
    __tablename__ = "users"
