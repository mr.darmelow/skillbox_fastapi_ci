from os import path

DEBUG: bool = True
URL: str = "http://localhost:8000"

TITLE: str = "Cookbook App"
DESCRIPTION: str = "bla...bla...bla..."
VERSION: str = "0.1.0"

DATABASE_PATH = path.join("database", "server_app.db")
DATABASE_URL = f"sqlite+aiosqlite:///{DATABASE_PATH}"
