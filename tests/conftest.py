import asyncio

from typing import AsyncGenerator, Generator
from asyncio import AbstractEventLoop

import pytest

from httpx import AsyncClient
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker, AsyncSession, AsyncEngine
from fastapi.testclient import TestClient

from main import application
from database.db import get_async_session
from database.models import BaseModel


DB_URL = "sqlite+aiosqlite:///test.db"

test_engine: AsyncEngine = create_async_engine(DB_URL)
async_session_maker = async_sessionmaker(bind=test_engine, expire_on_commit=False)
BaseModel.metadata.bind = test_engine


async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as async_session:
        yield async_session


application.dependency_overrides[get_async_session] = override_get_async_session


@pytest.fixture(scope="session")
async def async_client() -> AsyncGenerator[AsyncClient, None]:
    async with (AsyncClient(app=application, base_url="http://localhost:8000") as async_client):
        yield async_client


@pytest.fixture(autouse=True, scope="session")
async def prepare_db() -> AsyncGenerator[None, None]:
    async with test_engine.begin() as conn:
        await conn.run_sync(BaseModel.metadata.create_all)
    yield
    async with test_engine.begin() as conn:
        await conn.run_sync(BaseModel.metadata.drop_all)


# SETUP
@pytest.fixture(scope="session")
def event_loop():
    """Overrides pytest default function scoped event loop"""
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()
