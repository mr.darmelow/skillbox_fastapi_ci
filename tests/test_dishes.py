import json

from httpx import AsyncClient
from sqlalchemy import select, Select
from fastapi.testclient import TestClient

from conftest import async_session_maker
from routers.dishes.models import DishModel, DishStatisticsModel, DishViewModel
from main import application
from config import URL


fake_ingredients: list[str] = [
    "test ingredient # 1",
    "test ingredient # 2",
]

fake_data: dict[str, any] = {
    "name": "French's Mustard-Herb Roasted Chicken",
    "description": "Chicken",
    "ingredients": fake_ingredients,
    "cooking_time": 30
}

fake_views: list[int] = [30, 15, 30]
fake_cooking_time: list[int] = [40, 25, 50]

correct_sequence: list[int] = [1, 3, 2]


async def test_load_dishes():
    async with async_session_maker() as session:
        for i_index in range(3):
            instance = DishModel(
                name="Homemade Salsa #" + str(i_index),
                description="The Best Homemade Salsa Recipe! This homemade restaurant-style Salsa.",
                ingredients=json.dumps(fake_ingredients),
                cooking_time=fake_cooking_time[i_index]
            )
            session.add(instance)
            await session.flush()

            session.add(DishStatisticsModel(dish_id=instance.id, views_quantity=fake_views[i_index]))
            await session.commit()


def test_get_dishes() -> None:
    client = TestClient(app=application, base_url=URL)

    response = client.get("/dishes/")
    dishes = response.json()

    assert response.status_code == 200

    for i_index, i_value in enumerate(correct_sequence):
        assert dishes[i_index]["id"] == i_value


async def test_add_dish(async_client: AsyncClient) -> None:
    response = await async_client.post("/dishes/", json=fake_data)
    assert response.status_code == 201

    dish_id = response.json()["data"]["id"]

    async with async_session_maker() as session:
        dish_query: Select = select("*").where(DishModel.id == dish_id).select_from(DishModel)
        dish_response = await session.execute(dish_query)
        dish = dish_response.one_or_none()

        assert dish[0] == fake_data["name"]

        dish_statistics_statement: Select = select("*") \
            .where(DishStatisticsModel.dish_id == dish_id) \
            .select_from(DishStatisticsModel)

        dish_statistics_response = await session.execute(dish_statistics_statement)

        assert dish_statistics_response.one_or_none() is not None


async def test_get_dish(async_client: AsyncClient) -> None:
    user_id: int = 1
    dish_id: int = 1

    response = await async_client.get(f"/dishes/{dish_id}", params={"user_id": user_id})

    assert response.status_code == 200
    assert response.json()["name"] == "Homemade Salsa #0"

    async with async_session_maker() as session:
        query: Select = select("*") \
            .where(DishViewModel.dish_id == dish_id) \
            .where(DishViewModel.user_id == user_id) \
            .select_from(DishViewModel)

        response = await session.execute(query)
        assert response.one_or_none() is not None
